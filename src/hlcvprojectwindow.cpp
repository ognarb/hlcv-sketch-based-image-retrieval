/*
Copyright (C) 2019 by Carl Schwan <carl@carlschwan.eu>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy 
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// application headers
#include "hlcvprojectwindow.h"

#include "hlcvprojectview.h"
#include "hlcvprojectdebug.h"

// KF headers
#include <KActionCollection>
#include <KConfigDialog>

#include <QProcess>
#include <QByteArray>
#include <QList>
#include <QDebug>

hlcvprojectWindow::hlcvprojectWindow()
    : KXmlGuiWindow()
{
    m_fuzzysearchView = new FuzzySearchView(this);
    setCentralWidget(m_fuzzysearchView);

    setupGUI();
    setupConnections();
}

void hlcvprojectWindow::loadImagesFromSketch(QImage sketch)
{
    sketch.save(QString("/tmp/sketch.png"));
    QString program = "hellohello";
    QStringList arguments;
    arguments << QString("/tmp/sketch.png");

    QProcess *script = new QProcess(this);
    script->start(program, arguments);

    QByteArray byteArray;
    images.clear();
    do {
        byteArray = script->readLine(2000);
        qDebug() << byteArray << "heoi";
        images << QImage(QString(byteArray));
    } while (byteArray.size() > 0);

    for (const QImage &image : images) {
        qDebug() << image;
    }
}

void hlcvprojectWindow::setupConnections()
{
    connect(m_fuzzysearchView, &FuzzySearchView::sketchChanged, this, &hlcvprojectWindow::loadImagesFromSketch);
}
