/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2008-05-19
 * Description : Fuzzy search sidebar tab contents.
 *
 * Copyright (C) 2016-2018 by Mario Frank <mario dot frank at uni minus potsdam dot de>
 * Copyright (C) 2008-2019 by Gilles Caulier <caulier dot gilles at gmail dot com>
 * Copyright (C) 2008-2012 by Marcel Wiesweg <marcel dot wiesweg at gmx dot de>
 * Copyright (C) 2012      by Andi Clemens <andi dot clemens at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "fuzzysearchview.h"

// Qt includes

#include <QFrame>
#include <QImage>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QTime>
#include <QTimer>
#include <QToolButton>
#include <QApplication>
#include <QStyle>
#include <QLineEdit>
#include <QIcon>
#include <QMessageBox>
#include <QDebug>

// KDE includes

#include <KLocalizedString>

class Q_DECL_HIDDEN FuzzySearchView::Private
{

public:

    enum FuzzySearchTab
    {
        DUPLICATES = 0,
        SIMILARS,
        SKETCH
    };

public:

    explicit Private() :
        // initially be active to update sketch panel when the search list is restored
        active(false),
        fingerprintsChecked(false),
        resetButton(nullptr),
        saveBtnSketch(nullptr),
        undoBtnSketch(nullptr),
        redoBtnSketch(nullptr),
        saveBtnImage(nullptr),
        penSize(nullptr),
        resultsSketch(nullptr),
        imageWidget(nullptr),
        timerSketch(nullptr),
        timerImage(nullptr),
        nameEditSketch(nullptr),
        nameEditImage(nullptr),
        sketchWidget(nullptr)
    {
    }

    static const QString      configTabEntry;
    static const QString      configPenSketchSizeEntry;
    static const QString      configResultSketchItemsEntry;
    static const QString      configPenSketchHueEntry;
    static const QString      configPenSketchSaturationEntry;
    static const QString      configPenSkethValueEntry;
    static const QString      configSimilarsThresholdEntry;
    static const QString      configSimilarsMaxThresholdEntry;

    bool                      active;
    bool                      fingerprintsChecked;

    QColor                    selColor;

    QToolButton* resetButton;
    QToolButton* saveBtnSketch;
    QToolButton* undoBtnSketch;
    QToolButton* redoBtnSketch;
    QPushButton* saveBtnImage;

    QSpinBox* penSize;
    QSpinBox* resultsSketch;

    QLabel* imageWidget;

    QTimer* timerSketch;
    QTimer* timerImage;

    QLineEdit* nameEditSketch;
    QLineEdit* nameEditImage;

    SketchWidget* sketchWidget;
};

const QString FuzzySearchView::Private::configTabEntry(QLatin1String("FuzzySearch Tab"));
const QString FuzzySearchView::Private::configPenSketchSizeEntry(QLatin1String("Pen Sketch Size"));
const QString FuzzySearchView::Private::configResultSketchItemsEntry(QLatin1String("Result Sketch items"));
const QString FuzzySearchView::Private::configPenSketchHueEntry(QLatin1String("Pen Sketch Hue"));
const QString FuzzySearchView::Private::configPenSketchSaturationEntry(QLatin1String("Pen Sketch Saturation"));
const QString FuzzySearchView::Private::configPenSkethValueEntry(QLatin1String("Pen Sketch Value"));
const QString FuzzySearchView::Private::configSimilarsThresholdEntry(QLatin1String("Similars Threshold"));
const QString FuzzySearchView::Private::configSimilarsMaxThresholdEntry(QLatin1String("Similars Maximum Threshold"));

// --------------------------------------------------------

FuzzySearchView::FuzzySearchView(QWidget* const parent)
    : QScrollArea(parent),
      d(new Private)
{
    QWidget* sketchPanel = setupSketchPanel();

    QWidget* const mainWidget     = new QWidget(this);
    QVBoxLayout* const mainLayout = new QVBoxLayout();
    mainLayout->addWidget(sketchPanel);
    //mainLayout->addWidget(new QLabel(QString("TODO")));
    mainLayout->setContentsMargins(QMargins());
    mainLayout->setSpacing(0);
    mainWidget->setLayout(mainLayout);

    setWidget(mainWidget);

    // ---------------------------------------------------------------

    d->timerSketch = new QTimer(this);
    d->timerSketch->setSingleShot(true);
    d->timerSketch->setInterval(500);

    d->timerImage = new QTimer(this);
    d->timerImage->setSingleShot(true);
    d->timerImage->setInterval(500);

    setupConnections();

}

QWidget* FuzzySearchView::setupSketchPanel() const
{
    const int spacing = QApplication::style()->pixelMetric(QStyle::PM_DefaultLayoutSpacing);

    d->sketchWidget = new SketchWidget();

    // ---------------------------------------------------------------

    d->undoBtnSketch   = new QToolButton();
    d->undoBtnSketch->setAutoRepeat(true);
    d->undoBtnSketch->setIcon(QIcon::fromTheme(QLatin1String("edit-undo")));
    d->undoBtnSketch->setToolTip(i18n("Undo last draw on sketch"));
    d->undoBtnSketch->setWhatsThis(i18n("Use this button to undo last drawing action on sketch."));
    d->undoBtnSketch->setEnabled(false);

    d->redoBtnSketch   = new QToolButton();
    d->redoBtnSketch->setAutoRepeat(true);
    d->redoBtnSketch->setIcon(QIcon::fromTheme(QLatin1String("edit-redo")));
    d->redoBtnSketch->setToolTip(i18n("Redo last draw on sketch"));
    d->redoBtnSketch->setWhatsThis(i18n("Use this button to redo last drawing action on sketch."));
    d->redoBtnSketch->setEnabled(false);

    QLabel* const brushLabel = new QLabel(i18n("Pen:"));
    d->penSize               = new QSpinBox();
    d->penSize->setRange(1, 64);
    d->penSize->setSingleStep(1);
    d->penSize->setValue(10);
    d->penSize->setWhatsThis(i18n("Set here the brush size in pixels used to draw sketch."));

    QLabel* const resultsLabel = new QLabel(i18n("Items:"));
    d->resultsSketch           = new QSpinBox();
    d->resultsSketch->setRange(1, 50);
    d->resultsSketch->setSingleStep(1);
    d->resultsSketch->setValue(10);
    d->resultsSketch->setWhatsThis(i18n("Set here the number of items to find using sketch."));

    d->resetButton = new QToolButton();
    d->resetButton->setIcon(QIcon::fromTheme(QLatin1String("document-revert")));
    d->resetButton->setToolTip(i18n("Clear sketch"));
    d->resetButton->setWhatsThis(i18n("Use this button to clear sketch contents."));

    d->saveBtnImage = new QPushButton(i18n("Query"));

    QGridLayout* const settingsLayout = new QGridLayout();
    settingsLayout->addWidget(d->undoBtnSketch, 0, 0);
    settingsLayout->addWidget(d->redoBtnSketch, 0, 1);
    settingsLayout->addWidget(brushLabel,       0, 2);
    settingsLayout->addWidget(d->penSize,       0, 3);
    settingsLayout->addWidget(d->resetButton,      0, 4);
    settingsLayout->addWidget(resultsLabel,     1, 0);
    settingsLayout->addWidget(d->resultsSketch, 1, 1);
    settingsLayout->addWidget(d->saveBtnImage, 1, 2);
    settingsLayout->setColumnStretch(4, 10);
    settingsLayout->setContentsMargins(QMargins());
    settingsLayout->setSpacing(spacing);



    // ---------------------------------------------------------------

    QWidget* const mainWidget = new QWidget;
    QGridLayout* const mainLayout = new QGridLayout();
    mainLayout->addWidget(d->sketchWidget, 0, 0, 1, 3);
    mainLayout->addLayout(settingsLayout, 1, 0, 1, 3);
    mainLayout->setRowStretch(0, 10);
    mainLayout->setColumnStretch(1, 10);
    mainLayout->setContentsMargins(spacing, spacing, spacing, spacing);
    mainLayout->setSpacing(spacing);
    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

void FuzzySearchView::setupConnections()
{
    connect(d->penSize, SIGNAL(valueChanged(int)),
            d->sketchWidget, SLOT(setPenWidth(int)));

    connect(d->resultsSketch, SIGNAL(valueChanged(int)),
            this, SLOT(slotDirtySketch()));

    connect(d->resetButton, SIGNAL(clicked()),
            this, SLOT(slotClearSketch()));

    connect(d->sketchWidget, SIGNAL(signalPenSizeChanged(int)),
            d->penSize, SLOT(setValue(int)));

    connect(d->sketchWidget, SIGNAL(signalPenColorChanged(QColor)),
            this, SLOT(slotPenColorChanged(QColor)));

    connect(d->sketchWidget, SIGNAL(signalSketchChanged(QImage)),
            this, SLOT(slotDirtySketch()));

    connect(d->sketchWidget, SIGNAL(signalUndoRedoStateChanged(bool,bool)),
            this, SLOT(slotUndoRedoStateChanged(bool,bool)));

    connect(d->undoBtnSketch, &QToolButton::clicked,
            d->sketchWidget, &SketchWidget::slotUndo);

    connect(d->redoBtnSketch, &QToolButton::clicked,
            d->sketchWidget, &SketchWidget::slotRedo);

    connect(d->saveBtnImage, &QPushButton::clicked,
            [=]() {
                const QImage sketch = d->sketchWidget->sketchImage();
                emit this->sketchChanged(sketch);
            });

}

FuzzySearchView::~FuzzySearchView() = default;

void FuzzySearchView::slotUndoRedoStateChanged(bool hasUndo, bool hasRedo)
{
    d->undoBtnSketch->setEnabled(hasUndo);
    d->redoBtnSketch->setEnabled(hasRedo);
}

void FuzzySearchView::slotDirtySketch()
{
    if (d->active)
    {
        d->timerSketch->start();
    }
}

void FuzzySearchView::slotTimerSketchDone()
{
    slotCheckNameEditSketchConditions();
}

void FuzzySearchView::slotClearSketch()
{
    d->sketchWidget->slotClear();
    slotCheckNameEditSketchConditions();
}

void FuzzySearchView::slotCheckNameEditSketchConditions()
{
    qDebug() << "Save";
    if (!d->sketchWidget->isClear())
    {
        d->nameEditSketch->setEnabled(true);

        if (!d->nameEditSketch->text().isEmpty())
        {
            d->saveBtnSketch->setEnabled(true);
        }
    }
    else
    {
        d->nameEditSketch->setEnabled(false);
        d->saveBtnSketch->setEnabled(false);
    }
}
