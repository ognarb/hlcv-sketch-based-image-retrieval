/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2008-05-19
 * Description : Fuzzy search sidebar tab contents.
 *
 * Copyright (C) 2016-2018 by Mario Frank <mario dot frank at uni minus potsdam dot de>
 * Copyright (C) 2008-2019 by Gilles Caulier <caulier dot gilles at gmail dot com>
 * Copyright (C) 2008-2012 by Marcel Wiesweg <marcel dot wiesweg at gmx dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef DIGIKAM_FUZZY_SEARCH_VIEW_H
#define DIGIKAM_FUZZY_SEARCH_VIEW_H

// Qt includes

#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QScrollArea>
#include <QDropEvent>
#include <memory>

// Local includes 
#include "sketchwidget.h"

class QMimeData;
class QPixmap;

class FuzzySearchView : public QScrollArea
{
    Q_OBJECT

public:

    explicit FuzzySearchView(QWidget* const parent = nullptr);
    virtual ~FuzzySearchView();

    void doLoadState();
    void doSaveState();

private Q_SLOTS:

    void slotClearSketch();
    void slotTimerSketchDone();
    void slotDirtySketch();
    void slotUndoRedoStateChanged(bool, bool);
    void slotCheckNameEditSketchConditions();

signals:
    void sketchChanged(QImage sketch);

private:

    QWidget* setupSketchPanel() const;
    void setupConnections();
    class Private;
    std::unique_ptr<Private> const d;
};

#endif // DIGIKAM_FUZZY_SEARCH_VIEW_H
