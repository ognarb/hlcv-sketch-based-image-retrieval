/*
Copyright (C) 2019 by Carl Schwan <carl@carlschwan.eu>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy 
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>

#ifndef HLCVPROJECTVIEW_H
#define HLCVPROJECTVIEW_H

#include "fuzzysearch/sketchwidget.h"

/**
 * This class serves as the main window for hlcvproject.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Carl Schwan <carl@carlschwan.eu>
 * @version 0.1
 */
class hlcvprojectView : public QWidget
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    explicit hlcvprojectView(QWidget *parent);

    /**
     * Default Destructor
     */
    ~hlcvprojectView() override;

private:
    std::unique_ptr<SketchWidget> m_sketchWidget;
};

#endif // HLCVPROJECTVIEW_H

